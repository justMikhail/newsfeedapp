import React, { FC, useEffect } from 'react';
import { Switch, Route, useLocation } from 'react-router-dom';
import { ArticlesList } from './Components/ArticlesList/ArticlesList';
import { ArticleItem } from './Components/ArticleItem/ArticleItem';
import { PageLayout } from './Layouts/PageLayout/PageLayout';
import { AdminLayout } from './Layouts/AdminLayout/AdminLayout';
import { AdminArticleItem } from './Components/AdminArticleItem/AdminArticleItem';
import { AdminArticles } from './Components/AdminArticles/AdminArticles';
import { LoginContainer } from './features/auth/login/LoginContainer';
import { PrivateRoute } from './Components/PrivateRoute/PrivateRoute';

export const App: FC = () => {
  const { pathname } = useLocation();

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  return (
    <Switch>
      <Route path="/login">
        <PageLayout>
          <LoginContainer />
        </PageLayout>
      </Route>

      <PrivateRoute
        path="/admin"
        exact
      >
        <AdminLayout>
          <AdminArticles />
        </AdminLayout>
      </PrivateRoute>

      <PrivateRoute path="/admin/create">
        <AdminLayout>
          <AdminArticleItem />
        </AdminLayout>
      </PrivateRoute>

      <PrivateRoute path="/admin/edit/:id">
        <AdminLayout>
          <AdminArticleItem />
        </AdminLayout>
      </PrivateRoute>

      <Route path="/article/:id">
        <PageLayout>
          <ArticleItem />
        </PageLayout>
      </Route>

      <Route path="/:categoryId">
        <PageLayout>
          <ArticlesList />
        </PageLayout>
      </Route>

      <Route path="/">
        <PageLayout>
          <ArticlesList />
        </PageLayout>
      </Route>
    </Switch>
  );
};
