import React, { FC } from 'react';
import { beautifyDate } from '../../utils';
import './ArticleItemInfo.css';

type Props = {
  categoryName: string;
  date: string;
  sourceLink: string;
  sourceName: string;
  author: string;
};

export const ArticleItemInfo: FC<Props> = ({ categoryName, date, sourceLink, sourceName, author }) => {
  return (
    <div className="grid">
      <div className="article-item-info__category-container">
        <span className="article-category article__category">{categoryName}</span>

        {sourceLink && (
          <a
            className="article-item-info__link"
            href={sourceLink}
            target="_blank"
            rel="noreferrer"
          >
            Источник: {sourceName}
            {author && <span className="article-item-info__author">{author}</span>}
          </a>
        )}

        <span className="article-date article__date">{beautifyDate(date)}</span>
      </div>
    </div>
  );
};
