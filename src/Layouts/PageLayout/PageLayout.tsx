import React, { FC } from 'react';
import './PageLayout.css';
import { Navigation } from '../../Components/Navigation/Navigation';

export const PageLayout: FC = ({ children }) => {
  return (
    <>
      <header className="header">
        <div className="container">
          <Navigation
            placement="header"
            className="header__navigation"
          />
        </div>
      </header>

      <main>{children}</main>

      <footer className="footer">
        <div className="container">
          <Navigation
            placement="footer"
            className="footer__navigation"
          />
        </div>
      </footer>
    </>
  );
};
