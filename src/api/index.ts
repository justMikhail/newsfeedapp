import { initializeApp, FirebaseApp } from 'firebase/app';
import {
  collection,
  getDocs,
  getFirestore,
  addDoc,
  doc,
  getDoc,
  updateDoc,
  deleteDoc,
  query,
  orderBy,
  limit,
} from 'firebase/firestore';
import { getStorage, ref, uploadBytes, getDownloadURL } from 'firebase/storage';
import { PartnersArticle } from '../types';
import { getAuth } from 'firebase/auth';

const PARTNERS_POST_KEY = 'partners-posts';

export let firebaseApp: FirebaseApp;

export const initializeApi = (): FirebaseApp => {
  firebaseApp = initializeApp({
    apiKey: 'AIzaSyAhIXPx5Rz5Dt25cXk4TiRZ8guFwd91pCE',
    authDomain: 'news-feed-app-cc277.firebaseapp.com',
    projectId: 'news-feed-app-cc277',
    storageBucket: 'news-feed-app-cc277.appspot.com',
    messagingSenderId: '170155365083',
    appId: '1:170155365083:web:ca5fa2ae5f187d493e72fe',
  });

  getAuth(firebaseApp);
  getFirestore(firebaseApp);
  getStorage(firebaseApp);

  return firebaseApp;
};

export const getPartnersArticles = async (): Promise<PartnersArticle[]> => {
  const db = getFirestore();
  const querySnapshot = await getDocs(collection(db, PARTNERS_POST_KEY));

  const articles: PartnersArticle[] = [];

  try {
    querySnapshot.forEach((doc) => {
      const data = doc.data() as Omit<PartnersArticle, 'id'>;
      articles.push({
        id: doc.id,
        ...data,
      });
    });
  } catch (error) {
    return Promise.reject(error);
  }

  return articles;
};

export const createPartnerArticle = async (data: Omit<PartnersArticle, 'id' | 'created'>): Promise<any> => {
  try {
    const db = getFirestore();
    await addDoc(collection(db, PARTNERS_POST_KEY), data);
  } catch (error) {
    return Promise.reject(error);
  }
};

export const getPartnerArticle = async (id: string): Promise<PartnersArticle> => {
  const db = getFirestore();
  const docRef = doc(db, PARTNERS_POST_KEY, id);

  try {
    const docSnap = await getDoc(docRef);
    if (docSnap.exists()) {
      const data = docSnap.data() as Omit<PartnersArticle, 'id'>;

      return {
        id: docSnap.id,
        ...data,
      };
    } else {
      throw Error('Статья не найдена');
    }
  } catch (error) {
    return Promise.reject(error);
  }
};

export const updatePartnerArticle = async (id: string, data: Omit<PartnersArticle, 'id' | 'created'>): Promise<any> => {
  const db = getFirestore();
  const ref = doc(db, PARTNERS_POST_KEY, id);

  try {
    await updateDoc(ref, data);
  } catch (error) {
    return Promise.reject(error);
  }
};

export const deletePartnerArticle = async (id: string): Promise<any> => {
  const db = getFirestore();
  const ref = doc(db, PARTNERS_POST_KEY, id);

  try {
    await deleteDoc(ref);
  } catch (error) {
    return Promise.reject(error);
  }
};

export const uploadFile = async (file: File): Promise<string> => {
  const storage = getStorage();
  const storageRef = ref(storage, `${file.name}-${Date.now()}`);

  try {
    const snapshot = await uploadBytes(storageRef, file);
    const url = await getDownloadURL(snapshot.ref);
    return url;
  } catch (error) {
    return Promise.reject(error);
  }
};

export const getMainPartnerArticle = async (): Promise<PartnersArticle | null> => {
  const db = getFirestore();
  let article = null;

  try {
    const q = query(collection(db, PARTNERS_POST_KEY), orderBy('created', 'desc'), limit(1));
    const querySnapshot = await getDocs(q);

    querySnapshot.forEach((doc) => {
      const data = doc.data() as Omit<PartnersArticle, 'id'>;

      article = {
        id: doc.id,
        ...data,
      };
    });
  } catch (error) {
    return Promise.reject(error);
  }

  return article;
};
