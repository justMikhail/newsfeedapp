import React, { createContext, FC, useContext, useEffect, useState } from 'react';
import { getAuth, signInWithEmailAndPassword, browserSessionPersistence } from 'firebase/auth';
import { AuthContext } from './types';
import { FirebaseApp } from 'firebase/app';

type Props = {
  children: React.ReactNode;
  firebaseApp: FirebaseApp;
};

export const authContext = createContext<AuthContext>({
  isAuthenticated: null,
  loginWithEmailAndPassword: () => Promise.reject({}),
});

export const useAuthContext = (): AuthContext => {
  return useContext<AuthContext>(authContext);
};

export const AuthContextProvider: FC<Props> = ({ firebaseApp, children }) => {
  const [isAuthenticated, setIsAuthenticated] = useState<AuthContext['isAuthenticated']>(null);
  const [user, setUser] = useState<any>(null);
  const [auth] = useState(getAuth(firebaseApp));

  useEffect(() => {
    if (!auth) return;

    auth.setPersistence(browserSessionPersistence);
    auth.languageCode = 'ru';

    auth.onAuthStateChanged((user) => {
      if (user) {
        setUser(user);
        setIsAuthenticated(true);
      } else {
        setUser(null);
        setIsAuthenticated(false);
      }
    });
  }, [auth]);

  const loginWithEmailAndPassword = (email: string, password: string) => {
    return signInWithEmailAndPassword(auth, email, password)
      .then((result) => {
        // success auth
        return result;
      })
      .catch((error) => {
        // auth errors
        throw error;
      });
  };

  return (
    <authContext.Provider
      value={{
        isAuthenticated,
        user,
        loginWithEmailAndPassword,
      }}
    >
      {children}
    </authContext.Provider>
  );
};
