import { UserCredential } from 'firebase/auth';

type LoginWithEmailAndPasswordResult = UserCredential;

export type AuthContext = {
  isAuthenticated: boolean | null;
  user?: any;
  loginWithEmailAndPassword: (email: string, password: string) => Promise<LoginWithEmailAndPasswordResult>;
};
