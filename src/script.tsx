import React from 'react';
import ReactDOM from 'react-dom';
import './common.css';
import { App } from './App';
import { BrowserRouter as Router } from 'react-router-dom';
import { initializeApi } from './api';
import { AuthContextProvider } from '@features/auth/AuthContextProvider';

const firebaseApp = initializeApi();

ReactDOM.render(
  <AuthContextProvider firebaseApp={firebaseApp}>
    <Router>
      <App />
    </Router>
  </AuthContextProvider>,
  document.getElementById('root'),
);
